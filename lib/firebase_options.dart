// File generated by FlutterFire CLI.
// ignore_for_file: type=lint
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.

class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        return windows;
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyAUlDhiur-tOvnPXJlF5NKyTlqSk8PTI00',
    appId: '1:633697821628:web:f628344e87d3b8bf4481b9',
    messagingSenderId: '633697821628',
    projectId: 'chordify-task',
    authDomain: 'chordify-task.firebaseapp.com',
    databaseURL:
        'https://chordify-task-default-rtdb.asia-southeast1.firebasedatabase.app',
    storageBucket: 'chordify-task.appspot.com',
    measurementId: 'G-MM3R8CPTJ5',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyCBndvghJeGVwTOTy0xwSqPZm-Hhxz2zWc',
    appId: '1:633697821628:android:9b92821a2592227f4481b9',
    messagingSenderId: '633697821628',
    projectId: 'chordify-task',
    databaseURL:
        'https://chordify-task-default-rtdb.asia-southeast1.firebasedatabase.app',
    storageBucket: 'chordify-task.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyAYLxGV7csd2DKNze6EuU2D1k2UsrCwSZ4',
    appId: '1:633697821628:ios:6b8bf3cef0ac1ced4481b9',
    messagingSenderId: '633697821628',
    projectId: 'chordify-task',
    databaseURL:
        'https://chordify-task-default-rtdb.asia-southeast1.firebasedatabase.app',
    storageBucket: 'chordify-task.appspot.com',
    iosBundleId: 'com.example.chordifyTask',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyAYLxGV7csd2DKNze6EuU2D1k2UsrCwSZ4',
    appId: '1:633697821628:ios:6b8bf3cef0ac1ced4481b9',
    messagingSenderId: '633697821628',
    projectId: 'chordify-task',
    databaseURL:
        'https://chordify-task-default-rtdb.asia-southeast1.firebasedatabase.app',
    storageBucket: 'chordify-task.appspot.com',
    iosBundleId: 'com.example.chordifyTask',
  );

  static const FirebaseOptions windows = FirebaseOptions(
    apiKey: 'AIzaSyAUlDhiur-tOvnPXJlF5NKyTlqSk8PTI00',
    appId: '1:633697821628:web:31cc24287ede6b2b4481b9',
    messagingSenderId: '633697821628',
    projectId: 'chordify-task',
    authDomain: 'chordify-task.firebaseapp.com',
    databaseURL:
        'https://chordify-task-default-rtdb.asia-southeast1.firebasedatabase.app',
    storageBucket: 'chordify-task.appspot.com',
    measurementId: 'G-2N3J9TQZ8S',
  );
}
