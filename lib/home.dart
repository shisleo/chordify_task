import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyHome());

class MyHome extends StatelessWidget {
  const MyHome({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ConnectivityProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            dividerColor: Colors.transparent,
            scaffoldBackgroundColor: Colors.white,
            appBarTheme: const AppBarTheme(
                elevation: 1,
                surfaceTintColor: Colors.white,
                centerTitle: true,
                backgroundColor: Colors.white,
                titleTextStyle: TextStyle(fontSize: 18, color: Colors.black))),
        home: const MessageFormPage(),
      ),
    );
  }
}

class MessageFormPage extends StatefulWidget {
  const MessageFormPage({super.key});

  @override
  _MessageFormPageState createState() => _MessageFormPageState();
}

class _MessageFormPageState extends State<MessageFormPage> {
  final _formKey = GlobalKey<FormState>();
  String _firstName = '';
  String _lastName = '';
  String _email = '';
  String _message = '';

  @override
  void initState() {
    super.initState();
    _loadFormData();
  }

  Future<void> _loadFormData() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _firstName = prefs.getString('firstName') ?? '';
      _lastName = prefs.getString('lastName') ?? '';
      _email = prefs.getString('email') ?? '';
      _message = prefs.getString('message') ?? '';
    });
  }

  Future<void> _saveFormData() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('firstName', _firstName);
    prefs.setString('lastName', _lastName);
    prefs.setString('email', _email);
    prefs.setString('message', _message);
  }

  void _submitForm() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      final connectivity =
          Provider.of<ConnectivityProvider>(context, listen: false);
      if (connectivity.isConnected) {
        _saveToFirestore();
      } else {
        _saveLocally();
      }

      _formKey.currentState!.reset();
      _saveFormData();
    }
  }

  void _saveToFirestore() async {
    FirebaseFirestore.instance.collection('messages').add({
      'firstName': _firstName,
      'lastName': _lastName,
      'email': _email,
      'message': _message,
      'timestamp': FieldValue.serverTimestamp(),
    });
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Message saved online.')),
    );
  }

  void _saveLocally() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList('unsentMessages', [
      _firstName,
      _lastName,
      _email,
      _message,
    ]);
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
          content: Text(
        'Message saved locally.',
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 10),
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'User Details',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.black, fontSize: 28),
          ),
        ),
        body: ListView(children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('First Name',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold)),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            height: 45,
                            width: 170,
                            decoration: BoxDecoration(
                                color: const Color(0xffbFFFFFF),
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                    color: Color(0xffb0909094F), width: 1.5)),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: TextFormField(
                                initialValue: _firstName,
                                cursorColor: Colors.black,
                                decoration: const InputDecoration(
                                    labelText: 'Jane',
                                    labelStyle: TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                    isCollapsed: true,
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    border: InputBorder.none),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your first name';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _firstName = value!;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('Last Name',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold)),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            height: 45,
                            width: 170,
                            decoration: BoxDecoration(
                                color: const Color(0xffbFFFFFF),
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                    color: Color(0xffb0909094F), width: 1.5)),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: TextFormField(
                                initialValue: _lastName,
                                cursorColor: Colors.black,
                                decoration: const InputDecoration(
                                    labelText: 'Smirthton',
                                    labelStyle: TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                    filled: false,
                                    isCollapsed: true,
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    border: InputBorder.none),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your last name';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _lastName = value!;
                                },
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Text('Email Address',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        height: 45,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: const Color(0xffbFFFFFF),
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                                color: const Color(0xffb0909094F), width: 1.5)),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: TextFormField(
                            initialValue: _email,
                            cursorColor: Colors.black,
                            decoration: const InputDecoration(
                                labelText: 'jane@email.com',
                                labelStyle:
                                    TextStyle(color: Colors.grey, fontSize: 12),
                                isCollapsed: true,
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 5),
                                floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                border: InputBorder.none),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter your email';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              _email = value!;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Text('Your Message',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        height: 200,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: const Color(0xffbFFFFFF),
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                                color: Color(0xffb0909094F), width: 1.5)),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: TextFormField(
                            initialValue: _message,
                            cursorColor: Colors.black,
                            maxLines: 6,
                            decoration: const InputDecoration(
                                contentPadding: EdgeInsets.only(top: 10),
                                labelText: 'Enter Your Question or Message',
                                labelStyle:
                                    TextStyle(color: Colors.grey, fontSize: 12),
                                isCollapsed: true,
                                floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                border: InputBorder.none),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a message';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              _message = value!;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(8)),
                    width: double.infinity,
                    margin: const EdgeInsets.only(top: 30, left: 10, right: 10),
                    child: TextButton(
                        onPressed: _submitForm,
                        child: const Text(
                          "SUBMIT",
                          style: TextStyle(
                              fontSize: 18,
                              color: Color(0xffbFFFFFF),
                              fontWeight: FontWeight.bold),
                        )),
                  ),
                  const SizedBox(height: 40),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
                    child: Text('Submitted Messages',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(
                    height: 400,
                    child: StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('messages')
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Center(
                              child: CircularProgressIndicator());
                        }

                        if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                          return const Center(
                              child: Text('No messages submitted yet.'));
                        }
                        return ListView(
                          padding: const EdgeInsets.only(
                              top: 20, bottom: 20, left: 10, right: 10),
                          shrinkWrap: true,
                          children: snapshot.data!.docs.map((doc) {
                            final data = doc.data() as Map<String, dynamic>;
                            return Card(
                                color: Colors.white,
                                elevation: 1,
                                shadowColor: Colors.grey,
                                child: ListTile(
                                  titleAlignment: ListTileTitleAlignment.top,
                                  title: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 10, 0, 0),
                                    child: Text(data['message'],
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  subtitle: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 20, 0, 0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                            data['firstName'] +
                                                ' ' +
                                                data['lastName'],
                                            textAlign: TextAlign.left,
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold)),
                                        Text(data['email'],
                                            textAlign: TextAlign.left,
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400)),
                                      ],
                                    ),
                                  ),
                                ));
                          }).toList(),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          ),
        ]));
  }
}

class ConnectivityProvider with ChangeNotifier {
  bool isConnected = false;

  ConnectivityProvider() {
    _checkConnectivity();
    Connectivity().onConnectivityChanged.listen((result) {
      _updateConnectivityStatus(result);
      if (isConnected) {
        _syncLocalDataToFirestore();
      }
    });
  }

  void _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    _updateConnectivityStatus(connectivityResult);
  }

  void _updateConnectivityStatus(ConnectivityResult result) {
    isConnected = result != ConnectivityResult.none;
    notifyListeners();
  }

  void _syncLocalDataToFirestore() async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? unsentMessages = prefs.getStringList('unsentMessages');
    if (unsentMessages != null && unsentMessages.isNotEmpty) {
      await FirebaseFirestore.instance.collection('messages').add({
        'firstName': unsentMessages[0],
        'lastName': unsentMessages[1],
        'email': unsentMessages[2],
        'message': unsentMessages[3],
        'timestamp': FieldValue.serverTimestamp(),
      });
      prefs.remove('unsentMessages');
    }
  }
}
